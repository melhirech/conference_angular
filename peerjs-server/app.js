var express = require('express');
var app = express();
var cors = require('cors');
var ExpressPeerServer = require('peer').ExpressPeerServer;
const port = process.env.port || 3000;
app.use(cors());

app.get('/', function (req, res, next) {
  res.send('This is a peer.js server that supports SSL Enc');
});

var server = app.listen(port);

var options = {
  debug: true,
  allow_discovery: true
}

peerServer = ExpressPeerServer(server, options)
app.use('/peerjs', peerServer);

peerServer.on('connection', function (id) {
  console.log(id)
  console.log(server._clients)
});

server.on('disconnect', function (id) {
  console.log(id + " disconnected")
});