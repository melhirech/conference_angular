import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable()
export class FirebaseService {

  constructor () { }

  addPeer (roomId, peerId, index) {
    firebase.database().ref(`rooms/${roomId}/peers/${index}`).set({ peerId: peerId , isConnected: true});
  }

  removePeer(roomId, peers) {
    firebase.database().ref(`rooms/${roomId}/peers`).set(peers);
  }

  getPeers (roomId) {
    return firebase.database().ref(`rooms/${roomId}/peers`);
  }

  addQuestion (roomId, question) {
    return firebase.database().ref(`rooms/${roomId}/questions`).push(question);
  }

  getQuestions(roomId) {
    return firebase.database().ref(`rooms/${roomId}/questions`).orderByChild('votes');
  }

  insertMessage(roomId, questionId, index) {
    return firebase.database().ref(`rooms/${roomId}/questions/${questionId}`).child('comments').child(index);
  }

  getMessages (roomId, questionId) {
    return firebase.database().ref(`rooms/${roomId}/questions/${questionId}`).child('comments');
  }

  upvoteQuestion (roomId, questionId) {
    const databaseRef = firebase.database().ref(`rooms/${roomId}/questions/${questionId}`).child('votes');
    databaseRef.transaction((votes) => {
      return (votes || 0) + 1;
    });
  }
  downvoteQuestion(roomId, questionId) {
    const databaseRef = firebase.database().ref(`rooms/${roomId}/questions/${questionId}`).child('votes');
    databaseRef.transaction((votes) => {
      return (votes || 0) - 1;
    });
  }

}
