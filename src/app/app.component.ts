import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
constructor() {
  firebase.initializeApp({
    apiKey: "AIzaSyBuDd5KNB52Tmx3ZPRyBPEP-rma00unS8M",
    authDomain: "manifest-bit-166710.firebaseapp.com",
    databaseURL: "https://manifest-bit-166710.firebaseio.com",
    projectId: "manifest-bit-166710",
    storageBucket: "manifest-bit-166710.appspot.com",
    messagingSenderId: "113969886501"
  });
}
    }

