import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { FirebaseService } from '../../services/firebase.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {
  public username: string;
  public roomId: string;
  public loginError: string;
  public loginMode = false;

  constructor(private router: Router, private firebaseService: FirebaseService) {}

  ngOnInit() {
    firebase.database().ref('rooms').once('value')
      .then( snapshot => {
        console.log(snapshot.val());
      });
  }

  joinRoom (): void {
    if (this.username === undefined || this.roomId === undefined) {
          this.setError('Please Fill All Fields!');
    }
    firebase.database().ref(`rooms/${this.roomId}`).once('value')
      .then( (snapshot) => {
      if (snapshot.val()) {
        this.setError('Room already exists, please pick up another name.');
      } else {
        this.createRoom();
        this.router.navigate([`/${this.roomId}`], {queryParams: {username: this.username}});
      }
    });
  }

  createRoom (): void {
     const room = firebase.database().ref(`rooms/${this.roomId}`);
       room.set({admin: this.username});
  }

  setError(errorMsg) {
    this.loginError = errorMsg;
    setTimeout(() => {
      this.loginError = '';
    }, 2500);
  }

  changeMode() {
    this.loginMode = !this.loginMode;
  }

}
