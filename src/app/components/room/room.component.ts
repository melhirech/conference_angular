import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../../services/firebase.service';
import {forEach} from '@angular/router/src/utils/collection';

declare var Peer: any;
declare var jquery: any;
declare var $: any;


@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  public question: string;
  public description: string;
  public message: string;
  public selectedQ: any;
  public questions: Array<any>;
  public questionError: string = null;
  public messages: {username: string, message: string}[] = [];
  @ViewChild('chatContainer') private myScrollContainer: ElementRef;
  @ViewChild('videoStream1') videoStream1: any;
  @ViewChild('videoStream2') videoStream2: any;
  peer;
  mypeerid;
  remotePeers = 0;
  myStream: any;
  username: string;
  room: string;
  connectedPeers: any[];

  constructor(private route: ActivatedRoute, private firebaseService: FirebaseService) {

    this.peer = new Peer({
      host: 'peerjs-signal.herokuapp.com',
      port: '',
      path: '',
      debug: 3,
      config: {
        'iceServers': [
          { url: 'stun:stun1.l.google.com:19302' },
          {
            url: 'turn:numb.viagenie.ca',
            credential: 'muazkh', username: 'webrtc@live.com'
          }
        ]
      }
    });

    this.route.queryParams.subscribe(res => {
      this.username = res.username;
    });

    this.route.params.subscribe(res => {
      this.room = res.id;
    });
  }

  ngOnInit() {
    // Get question for this room
    this.firebaseService.getQuestions(this.room).on('value', quests => {
      if ( quests.val() === null || quests.val() === undefined ) {
        this.questions = [];
      } else {
        this.questions = [];
        quests.forEach(q => {
          this.questions.push({ ...q.val(), id: q.key });
          return false;
        });
      }
      this.questions = this.questions.reverse();
    });

    // Listen to new connected peers
    this.firebaseService.getPeers(this.room).on('value', peers => {
      this.connectedPeers = peers.val() || [];
      console.log(peers.val());
      peers.forEach( peer => {
        if(peer.val().peerId !== this.mypeerid) {
          this.videoconnect(peer.val().peerId);
        };
        return false;
      })
    });

    this.peer.on('open', (id) => {
      this.mypeerid = id;
      setTimeout(() => {
        this.firebaseService.addPeer(this.room, this.mypeerid, this.connectedPeers.length);
      }, 1000);
      console.log('connection opened: ', id);
    });

    this.peer.on('close', peerId => {
      this.connectedPeers.forEach( peer => {
        if (peer.peerId === this.mypeerid) {
          const index = this.connectedPeers.indexOf(peer);
          this.connectedPeers.splice(index, 1);
        }
      })
      this.firebaseService.removePeer(this.room, this.connectedPeers);
    });

    const video = this.videoStream1.nativeElement;
    const n = <any>navigator;
    const self = this;
    n.getUserMedia = (n.getUserMedia || n.webkitGetUserMedia || n.mozGetUserMedia || n.msGetUserMedia);
    n.getUserMedia({ video: true, audio: true }, ( mediaStream ) => {
      self.myStream = mediaStream;
      const localStream = self.videoStream2.nativeElement;
      localStream.src = URL.createObjectURL(self.myStream);
      localStream.play();
    }, (err) => {
      console.log('Failed to get stream', err);
    });

    this.peer.on('call', (call) => {
      call.answer(self.myStream);
      call.on('stream', (remotestream) => {
        video.src = URL.createObjectURL(remotestream);
        video.play();
      });
    });
  }

  addQuestion() {
    if (this.question === undefined || this.description === undefined) {
      this.questionError = 'Please Fill All Fields!'
      setTimeout(() => {
        this.questionError = null;
      }, 2500);
    }

      this.firebaseService.addQuestion(this.room, {
        content: this.question,
        description: this.description,
        timestamp: new Date().getTime(),
        votes: 0
      }).then(
      (result) => {
        this.question = '';
        this.description = '';
        $('#questionForm').modal('hide');
      });
  }

  onKey(event) {
    if (event.which === 13) {
      this.addMessage();
    }
  }

  addMessage() {
    if (this.message !== '' && this.message !== undefined) {
      const index = this.messages ? this.messages.length : 0;

      this.firebaseService.insertMessage(this.room, this.selectedQ.id, index).set({ username: this.username, message: this.message }).then(
        snapshot => this.scrollToBottom()
      )
      this.message = '';
    }
  }

  upvoteQuestion (id) {
    this.firebaseService.upvoteQuestion(this.room, id);
  }

  downvoteQuestion(id) {
  this.firebaseService.downvoteQuestion(this.room, id);
  };

  selectQuestion(question) {
    this.selectedQ = question;
    setTimeout(() => {
      this.scrollToBottom();
    }, 500);

    // Get messages for the selected question
    this.firebaseService.getMessages(this.room, this.selectedQ.id).on('value', snapshot => {
      this.messages = snapshot.val();
    });
  }

  // Getting video stream from browser
  videoconnect(id) {
    const video = this.videoStream1.nativeElement;
    const localStream = this.videoStream2.nativeElement;
    const localvar = this.peer;
    const n = <any>navigator;

    n.getUserMedia = (n.getUserMedia || n.webkitGetUserMedia || n.mozGetUserMedia || n.msGetUserMedia);

    n.getUserMedia({ video: true, audio: false }, function (stream) {
      localStream.src = URL.createObjectURL(stream);
      localStream.play();
      const call = localvar.call(id, stream);
      call.on('stream',
        (remotestream) => {
          video.src = URL.createObjectURL(remotestream);
          video.play();
        });
    },
      (err) => {
        console.log('Failed to get stream', err);
      });
  }

  // Scroll chat to bottom
  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

}
